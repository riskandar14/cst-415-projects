﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : Randomize
{
    //Objects Selected in Inspector
    public GameObject enemy1;
    public GameObject enemy2;
    public GameObject enemy3;
    public GameObject enemy4;
    public Text scoreText;

    private int score = 0;
    private int turn = 0;

    // Start is called before the first frame update
    void Awake()
    {
        Vector3.Distance(agent.transform.position, enemy1.transform.position);
        Vector3.Distance(agent.transform.position, enemy2.transform.position);
        Vector3.Distance(agent.transform.position, enemy3.transform.position);
        Vector3.Distance(agent.transform.position, enemy4.transform.position);
        Vector3.Distance(collectible.transform.position, enemy4.transform.position);
        scoreText.text = "Score: " + score;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(agent.transform.position, enemy1.transform.position) < 1.0)
        {
            score--;
            turn++;
            scoreText.text = "Score: " + score;
            randomStart();
            randomTarget();
            randomCollectible();
            resetEnemies();
            return;
        }
        else if (Vector3.Distance(agent.transform.position, enemy2.transform.position) < 1.0) {

            score--;
            turn++;
            scoreText.text = "Score: " + score;
            randomStart();
            randomTarget();
            randomCollectible();
            resetEnemies();
            return;
        }
        else if (Vector3.Distance(agent.transform.position, enemy3.transform.position) < 1.0)
        {

            score--;
            turn++;
            scoreText.text = "Score: " + score;
            randomStart();
            randomTarget();
            randomCollectible();
            resetEnemies();
            return;
        }
        else if (Vector3.Distance(agent.transform.position, enemy4.transform.position) < 1.0)
        {

            score--;
            turn++;
            scoreText.text = "Score: " + score;
            randomStart();
            randomTarget();
            randomCollectible();
            resetEnemies();
            return;
        }
        else if (Vector3.Distance(agent.transform.position, target.transform.position) < 1.0)
        {
            turn++;
            score++;
            speed = 10;
            randomTarget();
            scoreText.text = "Score: " + score;
            return;
        }
        else if (Vector3.Distance(agent.transform.position, collectible.transform.position) < 3.0)
        {
            score += 2;
            speed = 13;
            randomCollectible();
            randomPowerUp();
            scoreText.text = "Score: " + score;
            return;
        }
        else if (Vector3.Distance(enemy4.transform.position, collectible.transform.position) < 3.0)
        {
            score -= 2;
            speed = 7;
            randomCollectible();
            randomFakePowerUp();
            scoreText.text = "Score: " + score;
            return;
        }
        else if (Vector3.Distance(agent.transform.position, powerUp.transform.position) < 3.0) {
            powerUp.SetActive(false);
            powerUp.transform.position = new Vector3(17.5f, -5.0f, 17.5f);
            int buff = Random.Range(1, 3);
            switch(buff)
            {
                case 1:
                    resetEnemies();
                    break;
                case 2:
                    enemy2.SetActive(false);
                    break;
            }
        }
        else if (Vector3.Distance(agent.transform.position, fakePowerUp.transform.position) < 3.0)
        {
            fakePowerUp.SetActive(false);
            fakePowerUp.transform.position = new Vector3(17.5f, -5.0f, 17.5f);
            int debuff = Random.Range(1, 3);
            switch(debuff)
            {
                case 1:
                    randomStart();
                    break;
                case 2:
                    enemy2.SetActive(true);
                    break;
            }

        }
        else if (turn == 10 && score <= 0)
        {
            scoreText.text = "Enemy Wins!";
        }
        else if (turn == 10 && score > 0)
        {
            scoreText.text = "Agent Wins!";
        }
    }

    public void resetEnemies()
    {
        enemy1.transform.position = new Vector3(15.0f, 2.7f, 7.5f);
        enemy2.transform.position = new Vector3(30.0f, 2.7f, 7.5f);
        enemy3.transform.position = new Vector3(0.0f, 2.7f, 20.0f);
        enemy4.transform.position = new Vector3(27.5f, 2.7f, 17.5f);
    }
}
