﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Randomize : MonoBehaviour
{
    //Objects Selected in Inspector
    public GameObject target;
    public GameObject agent;
    public GameObject collectible;
    public GameObject powerUp;
    public GameObject fakePowerUp;

    static public int speed = 10;

    public void Start()
    {
        randomStart();
        randomTarget();
        randomCollectible();
    }

    public void randomStart()
    {
        int startLocation = Random.Range(1, 5);
        switch (startLocation)
        {
            case 1:
                agent.transform.position = new Vector3(-0.5f, 2.56f, -0.5f);
                break;
            case 2:
                agent.transform.position = new Vector3(-0.5f, 2.56f, 32.0f);
                break;
            case 3:
                agent.transform.position = new Vector3(30.0f, 2.56f, 30.0f);
                break;
            case 4:
                agent.transform.position = new Vector3(32.0f, 2.56f, -0.5f);
                break;
        }
    }

    public void randomTarget()
    {
        int targetLocation = Random.Range(1, 8);
        switch(targetLocation)
        {
            case 1:
                target.transform.position = new Vector3(2.5f, 2.51f, 12.5f);
                break;
            case 2:
                target.transform.position = new Vector3(15.0f, 2.51f, 30.0f);
                break;
            case 3:
                target.transform.position = new Vector3(12.5f, 2.51f, 12.5f);
                break;
            case 4:
                target.transform.position = new Vector3(20.0f, 2.51f, 27.5f);
                break;
            case 5:
                target.transform.position = new Vector3(30.0f, 2.51f, 17.5f);
                break;
            case 6:
                target.transform.position = new Vector3(27.5f, 2.51f, 5.0f);
                break;
            case 7:
                target.transform.position = new Vector3(17.5f, 2.51f, 0.0f);
                break;
        }
    }

    public void randomCollectible()
    {
        int collectibleLocation = Random.Range(1, 7);
        switch (collectibleLocation)
        {
            case 1:
                collectible.transform.position = new Vector3(0.0f, 0.0f, 5.0f);
                break;
            case 2:
                collectible.transform.position = new Vector3(17.5f, 0.0f, 10.0f);
                break;
            case 3:
                collectible.transform.position = new Vector3(20.0f, 0.0f, 22.5f);
                break;
            case 4:
                collectible.transform.position = new Vector3(25.0f, 0.0f, 10.0f);
                break;
            case 5:
                collectible.transform.position = new Vector3(27.5f, 0.0f, 2.5f);
                break;
            case 6:
                collectible.transform.position = new Vector3(12.5f, 0.0f, 27.5f);
                break;
        }
    }

    public void randomPowerUp()
    {
        powerUp.SetActive(true);
        fakePowerUp.SetActive(false);
        int powerUpLocation = Random.Range(1, 5);
        switch (powerUpLocation)
        {
            case 1:
                powerUp.transform.position = new Vector3(0.0f, 0.0f, 5.0f);
                break;
            case 2:
                powerUp.transform.position = new Vector3(17.5f, 0.0f, 10.0f);
                break;
            case 3:
                powerUp.transform.position = new Vector3(20.0f, 0.0f, 22.5f);
                break;
            case 4:
                powerUp.transform.position = new Vector3(25.0f, 0.0f, 10.0f);
                break;
        }
    }

    public void randomFakePowerUp()
    {
        fakePowerUp.SetActive(true);
        powerUp.SetActive(false);
        int fakePowerUpLocation = Random.Range(1, 5);
        switch (fakePowerUpLocation)
        {
            case 1:
                fakePowerUp.transform.position = new Vector3(0.0f, 0.0f, 5.0f);
                break;
            case 2:
                fakePowerUp.transform.position = new Vector3(17.5f, 0.0f, 10.0f);
                break;
            case 3:
                fakePowerUp.transform.position = new Vector3(20.0f, 0.0f, 22.5f);
                break;
            case 4:
                fakePowerUp.transform.position = new Vector3(25.0f, 0.0f, 10.0f);
                break;
        }
    }
}
