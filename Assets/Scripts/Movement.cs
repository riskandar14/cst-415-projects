﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : Randomize
{
    void Start()
    {
        
    }

    // Update is called once per frame  
    private void FixedUpdate()
    {
        Vector3 Movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        this.transform.position += Movement * speed * Time.deltaTime;
    }
}
