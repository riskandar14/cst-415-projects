## Unity AI Projects

**Author:**
Ryan Iskandar

**Course:**
CST-415

===============================

**Bayesian Network:**
This project simulates the addition of probabilites based on certain events occuring during gameplay. Power ups will spawn that can help or hurt the player,
but follow a pattern for advanced strategy.
The desired project scene is labeled Scene.unity and is located in Assets/Scenes folder.

===============================

**Hidden Markov Model:**
This project simulates the addition of probabilites and hidden events that change the games pace. When certain goals are reached or failed, the player will
need to adapt to changes within the game such as speed difference.
The desired project scene is labeled Scene.unity and is located in Assets/Scenes folder.

===============================

**Reinforcement Learning:**
This project simulates the addition of reinforcement learning by adding collectibles for the agent to grab to increase score. However, the higher the score,
the faster the enemies move (increased difficulty for good performance).
The desired project scene is labeled Scene.unity and is located in Assets/Scenes folder.

===============================

**Reward System:**
This project simulates the addition of rewarding the agent with points if it reaches the target location. However, if it encounters an enemy, then
it is reset and loses points.
The desired project scene is labeled Scene.unity and is located in Assets/Scenes folder.

===============================

**Enemy Tracking:**
This project simulates the addition of an enemy that will track the player AI and attempt to reach it before the player reaches the target location.
The desired project scene is labeled Scene.unity and is located in Assets/Scenes folder.

===============================

**Latin Square:**
This project simulates the Latin Square problem and creates a 10x10 grid of the playing area. Then it randomly chooses a target location for the 
player AI to move towards that (a space where a 10 would be) while avoiding obstacles. The target (green square) is placed at this location.
The desired project scene is labeled Scene.unity and is located in Assets/Scenes folder.

===============================

**A* Pathfinding:**
This project simulates the A* Pathfinding algorithm used by AI to find the best path from point A to point B while considering obstacles.
The desired project scene is labeled Scene.unity and is located in the Assets/Scenes folder.

===============================